INSTRUCTIONS FOR THE FILES FOR USE OF TIP4P/2005 WATER MODEL IN NAMD by Jordi Faraudo (ICMAB-CSIC)
--------------------------------------------------------------------------------------------------

The TIP4P-2005 was defined in J.L.F. Abascal; C. Vega, J. Chem. Phys., 123: 234505 (2005), for a comparison between different rigid models of water see Phys. Chem. Chem. Phys., Vol. 13, No. 44. (2011), pp. 19663-19688

Files included here:

* Topology file for generation of PSF structure ( use for example with autopsf in VMD)

TIP4P_2005.top

* Parameters file for use during Simulation with NAMD (compatible with CHARMM force field format)

par_TIP4P2005.inp


***** VERY IMPORTANT NOTES:

1) In NAMD configuration file ALWAYS use the option 

waterModel tip4

otherwise this water model is not recognized.

2) Versions of NAMD compatible with TIP4P water models:

NAMD 2.8 : no problems reported
NAMD 2.9 (standard version, April 2012): Undesired WARNINGS or ERRORS after reading psf files with lone pairs  depending on the machine, there are several reports on the NAMD discussion forums.
NAMD 2.9 CVS (Version Nightly Build): Works fine with tip4p, bugs were corrected! I recommend using this version (latest available).

*********** COMMENTS AND QUESTIONS?

Contact me at jfaraudo@icmab.es

--------------------------------

Acknowledgements: 
Thanks to A.C VilaVerde for discussions and sharing of data and files related to nonTIP3P water with namd
Financial support: FIS2011-13051-E Explora project 




