#!/usr/bin/tclsh

mol new OPC_WATER_No_Air.pdb autobonds no waitfor all

mol bondsrecalc top

topo retypebonds

topo bondtypenames

topo guessangles

topo guessdihedrals

mol reanalyze top

pbc set{0 0 0 40.0 40.0 80.0}

topo writelammpsdata Lammps_OPC_WATER_No_Air_data.imp full
